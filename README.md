# BankX 

BankX is a versatile mobile banking application developed using Flutter, offering users a comprehensive platform for managing their financial activities effortlessly.

## Features
- Account Management: Keep track of your account balances, transactions, and financial status in real-time.
- Transfer Funds: Transfer money between accounts securely and conveniently.
- Bill Payments: Pay bills, utilities, and other expenses directly from the app.
- Budget Tracker: Set budgets, categorize expenses, and monitor your spending habits.
- Customizable Alerts: Receive alerts for account activities, payments, and more.
- Enhanced Security: BankX employs advanced security measures to safeguard your financial data.


## Installation
1. Clone this repository to your local machine.
2. Run `flutter pub get` to install dependencies.
3. Connect your device/emulator and run `flutter run` to launch the app.

## Screenshots
![Home Screen](screenshots/home.png)
![Transactions](screenshots/transactions.png)
![ATM Locator](screenshots/atm_locator.png)

## Technologies Used
- Flutter
- Dart
- Firebase (for backend)

## Contributing
We welcome contributions! If you'd like to contribute to BankX, please fork this repository and submit a pull request.

## Contact
If you have any questions or feedback, feel free to reach out to us at [bankx@support.com](mailto:bankx@support.com).

Happy banking with BankX!